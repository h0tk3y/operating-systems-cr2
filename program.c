#include <errno.h>
#include <sys/wait.h>
#include <sys/types.h>

#define BUFFER_SIZE 1024
char buffer[BUFFER_SIZE];
char* read_pos = buffer;

#define FD_STDIN 0
#define FD_STDOUT 1

int checked_call(int result)
{
	if (result == -1)
		exit(errno);
	return result;
}

char* readline()
{
	for (char* pos = buffer; pos<read_pos; ++pos)
	   if (*pos == '\n')	
	   {
			*pos = '\0';
			char *result = malloc(pos - buffer + 1);
			memcpy(result, buffer, pos - buffer + 1);
			memcpy(buffer, pos+1, BUFFER_SIZE - (pos - buffer + 1));
			read_pos -= (pos - buffer + 1);
			return result; 
	   }
		else if (*pos == '\0')
			break;
	if (read_pos >= buffer + BUFFER_SIZE - 1)
		exit(1);
	int read_result = checked_call(read(FD_STDIN, read_pos, BUFFER_SIZE - (read_pos - buffer)));
	read_pos += read_result;
	return readline();
}

int execsh(char* cmd)
{
	execl("/bin/sh", "/bin/sh", "-c", cmd, 0);
}	

#define MAX_CHILDREN 256
int children[MAX_CHILDREN];
int* add_child = children;

void fork_exec_with_fds(char* cmd, int fd_in, int fd_out)
{
	int fork_result = checked_call(fork());
	if (fork_result == 0)
	{
		checked_call(dup2(fd_in, FD_STDIN));
		checked_call(dup2(fd_out, FD_STDOUT));
		if (fd_in != FD_STDIN)
			checked_call(close(fd_in));
		if (fd_out != FD_STDOUT)
			checked_call(close(fd_out));
		execsh(cmd);
	}
	if (add_child - children == MAX_CHILDREN)
		exit(1);
	*(add_child++) = fork_exec_with_fds;
}

int main(int argc, char* argv[])
{	
	int leftpipe[2],
		rightpipe[2];
	
	checked_call(pipe(leftpipe));
	checked_call(pipe(rightpipe));

	char* line;
	if (strlen(line = readline()) == 0)
		exit(0);
		
	fork_exec_with_fds(line, leftpipe[0], rightpipe[1]);
   	
	while (1) 
	{
		line = readline();
		if (strlen(line) == 0)
			break;
		if (line[0] == '<')
		{
			int fd_out = leftpipe[1];
			checked_call(pipe(leftpipe));
			fork_exec_with_fds(line + 3, leftpipe[0], fd_out);
		}
		else if (line[0] == '-')
		{
			int fd_in = rightpipe[0];
			checked_call(pipe(rightpipe));
			fork_exec_with_fds(line + 3, fd_in, rightpipe[1]);
		}
	}	

	fork_exec_with_fds("cat -", rightpipe[0], 1);
	//IDK, how to do the same for input, attaching "cat -" from the left doesn't work

	for (int* child = children; child<add_child; ++child)
		waitpid(child, NULL, 0);
}		
